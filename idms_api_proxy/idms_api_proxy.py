from flask import Flask, make_response, request
from flask_htpasswd import HtPasswdAuth

from werkzeug.middleware.proxy_fix import ProxyFix

from collections import OrderedDict
from time import monotonic as monotime

from urllib.parse import urlparse, parse_qs

from gevent.threadpool import ThreadPool
from requests import get as request_get

from json import dumps as json_dump

from os import environ
from sys import setswitchinterval

import logging

# Basic logging config
log_format = (
    '[%(asctime)s] [%(process)d] [%(levelname)s] '
    '[%(filename)s:%(lineno)d - %(funcName)s] '
    '%(message)s'
)
log_date_format = '%Y-%m-%d %H:%M:%S %z'

reject_logger = 'idms_api_proxy_reject_logger'

reject_log_file = '/var/log/idms_api_proxy/reject_log'
reject_log_handler = logging.FileHandler(reject_log_file)
reject_log_formatter = logging.Formatter(fmt=log_format,
                                         datefmt=log_date_format)
reject_log_handler.setFormatter(reject_log_formatter)

reject_log = logging.getLogger(reject_logger)
reject_log.setLevel(logging.INFO)
reject_log.addHandler(reject_log_handler)
reject_log.propagate = False

# Set the thread switch interval to 25 microseconds
setswitchinterval(0.000025)

# Seconds to block a failing IP
fail_block_seconds = 300

# Number of failed attempts after which to block
max_fail_attempts = 5


class BlockedIPCache:
    def __init__(self, maxsize=2048):
        self.maxsize = maxsize
        self.cache = OrderedDict()

    def check(self, ip):
        cache = self.cache
        record = cache.get(ip)

        if record:
            (total_attempts, last_attempt_expire_time) = record
            current_time = monotime()

            if (
                    total_attempts >= max_fail_attempts and
                    last_attempt_expire_time < current_time
            ):
                cache[ip] = (total_attempts + 1,
                             current_time + fail_block_seconds)
                return True

        # Record not found, total attempts below threshold, or
        # last attempt time has passed.
        return False

    def update(self, ip):
        cache = self.cache
        current_time = monotime()
        record = cache.get(ip)

        if record:
            (total_attempts, last_attempt_expire_time) = record
            if last_attempt_expire_time < current_time:
                total_attempts = 0
            cache[ip] = (total_attempts + 1,
                         current_time + fail_block_seconds)
        else:
            if len(cache) >= self.maxsize:
                cache.popitem(False)
            cache[ip] = (1,
                         current_time + fail_block_seconds)

    def remove(self, ip):
        cache = self.cache
        record = cache.get(ip)

        if record:
            del cache[ip]


# Worker pool for processing IdMS requests
requestPoolSize = 4
requestWorkerPool = ThreadPool(requestPoolSize)


# The body of the IdMS proxy authentication app
app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = '/opt/idms_api_proxy/credentials/htpasswd'
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

htpasswd = HtPasswdAuth(app)

banList = BlockedIPCache()


@app.route('/auth-idms', methods=['GET'])
def check_idms_request_access():
    client_ip = request.remote_addr

    # First, check if client is already banned.
    if banList.check(client_ip):
        reject_log.info(f'{client_ip} in penalty box.')
        return make_response('Nope.\n', 403)

    # Next, check that the right method was used.
    if request.method != 'GET':
        banList.update(client_ip)
        reject_log.info(f'{client_ip} used wrong method.')
        return make_response('Wrong method.\n', 403)

    # Next, check Basic auth.
    try:
        is_valid, user = htpasswd.authenticate()
    except Exception:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} provided wrong credentials.')
        return make_response('Rolling in the fail again, I see.\n', 401)

    if not is_valid:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} provided wrong credentials.')
        return make_response('Guess again.\n', 401)

    # Next, parse request parameters and check both those and URI.
    #
    # We are checking:
    # 1) That the path is as expected.
    # 2) That all expected query parameters are present.
    # 3) That the 'attributes' parameter contains the expected ones.
    # 4) That the 'identifierAttribute' parameter contains the expected one.
    #
    # All of these must be correct, before we grant access.

    uri_params = request.headers.get('X-Origin-URI')
    original_qs = urlparse(uri_params)
    parsed_qs = parse_qs(original_qs.query)

    idAttr = parsed_qs.get('identifierAttribute')
    duid = parsed_qs.get('identifier')
    attrs = parsed_qs.get('attributes')

    if original_qs.path != '/idm-ws/user/findByIdentifier':
        banList.update(client_ip)
        reject_log.info(f'{client_ip} used wrong path.')
        return make_response('Not even wrong.\n', 403)

    if not duid:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} did not provide a DUID.')
        return make_response('Really?\n', 403)

    if not idAttr or idAttr[0] != 'USR_LOGIN':
        banList.update(client_ip)
        reject_log.info((f'{client_ip} did not provide '
                         f'an identifier attribute.'))
        return make_response('Dropped on your head, were you?\n', 403)

    if not (
            attrs is not None and
            'USR_FIRST_NAME' in attrs[0] and
            'USR_LAST_NAME' in attrs[0] and
            'USR_LOGIN' in attrs[0] and
            'USR_EMAIL' in attrs[0]
    ):
        banList.update(client_ip)
        reject_log.info(f'{client_ip} queried wrong attributes.')
        return make_response('I never liked you.\n', 403)
    else:
        # We finally got it right.
        banList.remove(client_ip)
        return make_response('OK', 202)

    # A fail-safe we should never reach.
    reject_log.info((f'{client_ip} hit the fail-safe; '
                     f'this should be impossible.'))
    return make_response('How did you get here?.\n', 403)


@app.route('/auth-dco', methods=['GET'])
def check_dco_request_access():
    client_ip = request.remote_addr

    # First, check if client is already banned.
    if banList.check(client_ip):
        reject_log.info(f'{client_ip} in penalty box.')
        return make_response('Nope.\n', 403)

    # Next, check that the right method was used.
    if request.method != 'GET':
        banList.update(client_ip)
        reject_log.info(f'{client_ip} used wrong method.')
        return make_response('Wrong method.\n', 403)

    # Next, check Basic auth.
    try:
        is_valid, user = htpasswd.authenticate()
    except Exception:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} provided wrong credentials.')
        return make_response('Rolling in the fail again, I see.\n', 401)

    if not is_valid:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} provided wrong credentials.')
        return make_response('Guess again.\n', 401)

    # Next, we check that the path is as expected.
    #
    # This also must be correct, before we grant access.

    uri_params = request.headers.get('X-Origin-URI')
    original_qs = urlparse(uri_params)

    if '/dcoapi/auth/v2/Customer/cardNumber' not in original_qs.path:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} used wrong path.')
        return make_response('Not even wrong.\n', 403)

    # We finally got it right.
    banList.remove(client_ip)
    return make_response('OK', 202)


DCO_KEY = environ.get('DCO_KEY')
DCO_URL = 'https://dco31.oit.duke.edu/dcoapi/auth/v2/Customer/cardNumber/'


def perform_dco_query(card_number):
    query_url = f'{DCO_URL}{card_number}'
    headers = {'X-API-Key': DCO_KEY,
               'Accept-Charset': 'UTF-8'}

    resp = None
    try:
        resp = request_get(query_url, headers=headers, timeout=4)
    except Exception:
        print('DukeCard Office API endpoint not responding.')

    return resp


@app.route('/dcoapi/auth/v2/Customer/cardNumber/<card_number_str>',
           methods=['GET'])
def get_dco_customer_data(card_number_str):
    if len(card_number_str) == 10:
        card_number_str = card_number_str[:-1]
    card_number = int(card_number_str)
    resp = requestWorkerPool.spawn(
        perform_dco_query,
        card_number
    ).get()

    if resp is None:
        print('DukeCard Office endpoint failed to respond.')
        return make_response('Request to backend failed.\n', 502)

    status_code = resp.status_code
    dco_result = None
    if status_code == 200:
        try:
            dco_result = resp.json()
        except Exception:
            print('DukeCard Office endpoint response was not JSON.')

    resp.close()

    if dco_result is not None:
        dco_duid = dco_result['CustomerNumber']
        while len(dco_duid) < 7:
            dco_duid = f'0{dco_duid}'

        dco_result['CustomerNumber'] = dco_duid
        dco_result['succeeded'] = True
        headers = {'Content-Type': 'application/json'}
        return make_response(json_dump(dco_result),
                             200, headers)

    return make_response('Requested card number did not provide a result.',
                         404)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
