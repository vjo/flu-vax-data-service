console.log('Loaded Duke 1D handlers from server.');

async function scanCode()
{
    var scannerElement = null;
    while(!scannerElement) {
        setTimeout(console.log('No scanner element found yet.'), 250);
        scannerElement = document.getElementById('codescanner');
    }
    console.log('Found scanner element');
    const scannerMessagesElement = document.getElementById('scannerMessages');
    const scannerContainerElement = document.getElementById('scannerContainer');
    const scannedPayload = document.getElementById('scannedPayload');

    console.log('About to create BrowserMultiFormatReader');
    const hints = new Map();
    const formats = [ZXing.BarcodeFormat.CODE_39];
    hints.set(ZXing.DecodeHintType.TRY_HARDER, true);
    hints.set(ZXing.DecodeHintType.POSSIBLE_FORMATS, formats);
    const Scanner = new ZXingBrowser.BrowserMultiFormatReader(hints);

    console.log('Starting code scan from camera...');
    const controls =
          await Scanner.decodeFromVideoDevice(undefined,
                                              scannerElement,
                                              (result, error, controls) => {
                                                  // use the result and error values to choose your actions
                                                  // you can also use controls API in this scope like the controls
                                                  // returned from the method.
                                                  if (result)
                                                  {
                                                      controls.stop();

                                                      scannedPayload.value = result.text;
                                                      const event = new Event('input', {
                                                          bubbles: true,
                                                          cancelable: true,
                                                      });
                                                      scannedPayload.dispatchEvent(event);

                                                      scannerContainerElement.textContent = 'Code successfully scanned!';
                                                      scannerContainerElement.style.backgroundColor = 'green';
                                                      scannerContainerElement.style.color = 'black';
                                                  }

                                                  if (error instanceof ZXing.NotFoundException) {
                                                      console.log('No code found.')
                                                  }
                                                  if (error instanceof ZXing.ChecksumException) {
                                                      console.log('Code read value was not valid.')
                                                  }
                                                  if (error instanceof ZXing.FormatException) {
                                                      console.log('Code has invalid format.')
                                                  }
                                              }).catch(function(err) {
                                                  scannerContainerElement.textContent = 'Unable to scan a QR code. Has access been provided to the device camera?';
                                                  scannerContainerElement.style.backgroundColor = 'red';
                                                  scannerContainerElement.style.color = 'black';
                                              });

    console.log('Scanner setup complete; we should be live.');
}

scanCode();
