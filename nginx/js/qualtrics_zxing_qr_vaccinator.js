console.log('Loaded Duke QR handler for vaccinator from server.');

async function scanCode()
{
    var scannerElement = null;
    while(!scannerElement) {
        setTimeout(console.log('No scanner element found yet.'), 250);
        scannerElement = document.getElementById('codescanner');
    }
    console.log('Found scanner element');
    const scannerMessagesElement = document.getElementById('scannerMessages');
    const scannerContainerElement = document.getElementById('scannerContainer');
    const scannedPayload = document.getElementById('scannedPayload');

    console.log('About to create BrowserQRCodeReader');
    const hints = new Map();
    hints.set(ZXing.DecodeHintType.TRY_HARDER, true);
    const Scanner = new ZXingBrowser.BrowserQRCodeReader(hints);

    console.log('Starting code scan from camera...');
    const controls =
          await Scanner.decodeFromVideoDevice(undefined,
                                              scannerElement,
                                              (result, error, controls) => {
                                                  // use the result and error values to choose your actions
                                                  // you can also use controls API in this scope like the controls
                                                  // returned from the method.
                                                  if (result)
                                                  {
                                                      const result_text = result.text;
                                                      const qr_payload = new URL(result_text);
                                                      const qr_pathparts = (qr_payload.pathname).split('/');
                                                      if (qr_pathparts[4] == 0)
                                                      {
                                                          controls.stop();
                                                          scannerMessagesElement.textContent = '';

                                                          scannedPayload.value = result_text;
                                                          const event = new Event('input', {
                                                              bubbles: true,
                                                              cancelable: true,
                                                          });
                                                          scannedPayload.dispatchEvent(event);

                                                          scannerContainerElement.textContent = 'Code successfully scanned!';
                                                          scannerContainerElement.style.backgroundColor = 'green';
                                                          scannerContainerElement.style.color = 'black';
                                                      }
                                                      else
                                                      {
                                                          scannerMessagesElement.textContent = 'Wrong type of QR code scanned. Please scan a vaccinator\'s QR code.';
                                                          scannerMessagesElement.style.backgroundColor = 'red';
                                                          scannerMessagesElement.style.color = 'black';
                                                      }
                                                  }

                                                  if (error instanceof ZXing.NotFoundException) {
                                                      console.log('No code found.')
                                                  }
                                                  if (error instanceof ZXing.ChecksumException) {
                                                      console.log('Code read value was not valid.')
                                                  }
                                                  if (error instanceof ZXing.FormatException) {
                                                      console.log('Code has invalid format.')
                                                  }
                                              }).catch(function(err) {
                                                  scannerContainerElement.textContent = 'Unable to scan a QR code. Has access been provided to the device camera?';
                                                  scannerContainerElement.style.backgroundColor = 'red';
                                                  scannerContainerElement.style.color = 'black';
                                              });

    console.log('Scanner setup complete; we should be live.');
}

scanCode();
