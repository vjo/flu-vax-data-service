from flask import Flask, make_response, request
from flask_htpasswd import HtPasswdAuth

from werkzeug.middleware.proxy_fix import ProxyFix

from os import environ
from os import path as os_path
from os import remove as os_remove
from sys import setswitchinterval
from time import sleep

from json import dumps as json_dump

from qrcode import QRCode
from uuid import uuid4

from base64 import urlsafe_b64encode, urlsafe_b64decode, b64encode
from enum import IntEnum

from datetime import datetime, timedelta

from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP
from ssl import create_default_context as ssl_create_context

from weasyprint import HTML, CSS

from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy import create_engine as sa_create_engine
from sqlalchemy import Column, Identity, Integer, String
from sqlalchemy import DateTime, ForeignKey
from sqlalchemy import select
from sqlalchemy import desc

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

from gevent import spawn as gevent_spawn
from gevent.queue import Queue as gQueue
from gevent.threadpool import ThreadPool

from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

from PIL import Image, ImageFont, ImageDraw

from traceback import format_exc

import logging
import pytz

from urllib.parse import unquote, urlparse
from pathlib import PurePosixPath
from requests import get as request_get
from requests.auth import HTTPBasicAuth

# Basic logging config
log_format = (
    '[%(asctime)s] [%(process)d] [%(levelname)s] '
    '[%(filename)s:%(lineno)d - %(funcName)s] '
    '%(message)s'
)
log_date_format = '%Y-%m-%d %H:%M:%S %z'

# Base logging
base_logger = 'qr_vax_data_logger'

base_log_handler = logging.StreamHandler()
base_log_formatter = logging.Formatter(fmt=log_format, datefmt=log_date_format)
base_log_handler.setFormatter(base_log_formatter)

base_log = logging.getLogger(base_logger)
base_log.setLevel(logging.INFO)
base_log.addHandler(base_log_handler)
base_log.propagate = False

# What hostname should we provide in the QR codes?
qr_hostname = environ.get('QR_HOSTNAME')
if not qr_hostname:
    base_log.warning('QR_HOSTNAME is unset!!!')

# What environment are we operating in?
usage_context = environ.get('USAGE_CONTEXT')
if not usage_context:
    base_log.warning('USAGE_CONTEXT is unset!!!')

# Who should get Clinic QR PDFs?
clinic_distribution_list = environ.get('CLINIC_DISTRIBUTION_LIST')

# Hours for which a vaccinatee survey result is valid
survey_valid_hours = environ.get('CONSENT_VALIDITY_HOURS')
if not survey_valid_hours:
    survey_valid_hours = 48

# PDF code setup
pdf_base_path = '/opt/qr_vax_data/pdfs/'
pdf_footer_logo_file = f'{pdf_base_path}templates/duke_health.png'

# QR code setup
qr_base_path = '/opt/qr_vax_data/qr_codes/'
qr_font = '/opt/qr_vax_data/fonts/NotoSans-Bold.ttf'
qr_topFont = ImageFont.truetype(qr_font, 80)
qr_bottomFont = ImageFont.truetype(qr_font, 40)

# Vaccination branding
band_aid_file = f'{qr_base_path}templates/band_aid_heart.png'


class QRCodeType(IntEnum):
    Vaccinator = 0
    Vaccinatee = 1
    Clinic = 2


# Set the thread switch interval to 25 microseconds
setswitchinterval(0.000025)

# Queues and worker pools for processing emails asynchronously
mail_pool_size = 3

qr_mail_queue = gQueue()
qr_mail_worker_pool = ThreadPool(mail_pool_size)
report_mail_queue = gQueue()
report_mail_worker_pool = ThreadPool(mail_pool_size)
clinic_mail_queue = gQueue()
clinic_mail_worker_pool = ThreadPool(mail_pool_size)


def qr_mail_queue_processor():
    while True:
        (recipient_email, recipient_name,
         qr_timestamp, qr_type,
         qr_path, pdf_path) = qr_mail_queue.get()

        qr_mail_worker_pool.spawn(mail_qr_code,
                                  recipient_email, recipient_name,
                                  qr_timestamp, qr_type,
                                  qr_path, pdf_path)
        # Under gevent, keep this loop from being too tight.
        sleep(0)


def report_mail_queue_processor():
    while True:
        (recipient_email, recipient_name,
         pdf_path) = report_mail_queue.get()

        report_mail_worker_pool.spawn(mail_report,
                                      recipient_email, recipient_name,
                                      pdf_path)
        # Under gevent, keep this loop from being too tight.
        sleep(0)


def clinic_mail_queue_processor():
    while True:
        (site_code, pdf_path) = clinic_mail_queue.get()

        clinic_mail_worker_pool.spawn(mail_clinic_pdf,
                                      site_code,
                                      pdf_path)
        # Under gevent, keep this loop from being too tight.
        sleep(0)


gevent_spawn(qr_mail_queue_processor)
gevent_spawn(report_mail_queue_processor)
gevent_spawn(clinic_mail_queue_processor)

# Time formatting for emails
time_format = '%I:%M:%S %p %Z, %A %m-%d-%Y'
local_timezone = pytz.timezone('US/Eastern')

# SMTP config
smtp_server = 'smtp.duke.edu'
smtp_port = 25
smtp_sender = 'Duke Community Vaccination Service <no-reply@duke.edu>'
smtp_retry_global_count = 5
smtp_retry_global_sleep = 5

# SMTP logging
smtp_logger = 'qr_vax_data_smtp_logger'

smtp_log_file = '/var/log/qr_vax_data/smtp_log'
smtp_log_handler = logging.FileHandler(smtp_log_file)
smtp_log_formatter = logging.Formatter(fmt=log_format, datefmt=log_date_format)
smtp_log_handler.setFormatter(smtp_log_formatter)

smtp_log = logging.getLogger(smtp_logger)
smtp_log.setLevel(logging.INFO)
smtp_log.addHandler(smtp_log_handler)
smtp_log.propagate = False


def mail_qr_code(smtp_recipient, smtp_recipient_name,
                 qr_timestamp, qr_type, qr_path, pdf_path):
    generated_dt = qr_timestamp.replace(tzinfo=pytz.utc)
    generated_timestamp = generated_dt.astimezone(local_timezone)
    timestamp_str = generated_timestamp.strftime(time_format)

    subject_str = 'Your Vaccination Workflow QR Code'
    if usage_context and (usage_context.lower() != 'prod'):
        subject_str = (
            f'{usage_context} - '
            f'{subject_str}'
        )

    message = MIMEMultipart()
    message['From'] = smtp_sender
    message['To'] = smtp_recipient
    message['Subject'] = subject_str

    if smtp_recipient_name:
        smtp_recipient_name = f' {smtp_recipient_name}'

    body = (
        f'Hello'
        f'{smtp_recipient_name},\n\n'
        f'A copy of the QR code generated by your vaccination '
        f'survey responses is attached to this email.\n\n'
        f'Your QR code will be needed to complete your vaccination.\n'
        f'Please have it available when you arrive at a '
        f'vaccination clinic.\n\n'
        f'This QR code was generated for you at:\n'
        f'{timestamp_str}\n\n'
        f'It is valid for {survey_valid_hours} hours from generation time.\n'
        f'If your code expires, please complete the survey again '
        f'to obtain a new QR code.\n\n'
        f'A PDF containing this information has also '
        f'been attached, for ease of printing.\n\n'
        f'This email has been automatically generated.\n'
        f'DO NOT REPLY TO THIS EMAIL.\n'
    )
    if qr_type == QRCodeType.Vaccinator:
        body = (
            f'Hello'
            f'{smtp_recipient_name},\n\n'
            f'A copy of the QR code generated for you as a vaccinator '
            f'is attached to this email.\n\n'
            f'This QR code was generated for you at:\n'
            f'{timestamp_str}\n\n'
            f'A PDF containing this information has also '
            f'been attached, for ease of printing.\n\n'
            f'This email has been automatically generated.\n'
            f'DO NOT REPLY TO THIS EMAIL.\n'
        )

    if usage_context and (usage_context.lower() != 'prod'):
        body = (
            f'{usage_context}\n\n'
            f'{body}'
        )

    message.attach(MIMEText(body, 'plain'))

    with open(qr_path, 'rb') as qr_img:
        img_part = MIMEBase('image', 'png')
        img_part.add_header('Content-Transfer-Encoding', 'base64')
        encoded_img = b64encode(qr_img.read())
        img_part.set_payload(encoded_img, 'ascii')
    img_part.add_header('Content-Disposition',
                        'attachment; filename="vaccination_qr_code.png"')
    message.attach(img_part)

    with open(pdf_path, 'rb') as pdf:
        pdf_part = MIMEBase('application', 'pdf')
        pdf_part.add_header('Content-Transfer-Encoding', 'base64')
        encoded_pdf = b64encode(pdf.read())
        pdf_part.set_payload(encoded_pdf, 'ascii')
    pdf_part.add_header('Content-Disposition',
                        'attachment; filename="vaccination_qr_code.pdf"')
    message.attach(pdf_part)

    text = message.as_string()

    fail_msg = f'Recipient QR code send failed to {smtp_recipient}'
    if qr_type == QRCodeType.Vaccinator:
        fail_msg = (
            f'Vaccinator QR code send failed to '
            f'{smtp_recipient}'
        )

    ssl_context = ssl_create_context()
    smtp_retry_count = smtp_retry_global_count
    server = None
    while smtp_retry_count > 0:
        try:
            server = SMTP(smtp_server, smtp_port)
        except Exception as e:
            smtp_log.warning((f'Exception occurred during SMTP setup: '
                              f'{type(e)}'))
            server = None

        if server:
            try:
                server.starttls(context=ssl_context)
                server.sendmail(smtp_sender, smtp_recipient, text)

                success_msg = f'Recipient QR code sent to {smtp_recipient}'
                if qr_type == QRCodeType.Vaccinator:
                    success_msg = (
                        f'Vaccinator QR code sent to '
                        f'{smtp_recipient}'
                    )
                smtp_log.info(success_msg)
                smtp_retry_count = 0
            except Exception as e:
                smtp_log.warning(fail_msg)
                smtp_log.warning((f'Exception resulting in failure to send: '
                                  f'{type(e)}'))
                smtp_retry_count -= 1
                smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
                continue
            finally:
                server.quit()
        else:
            smtp_log.warning(fail_msg)
            smtp_retry_count -= 1
            smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
            sleep(smtp_retry_global_sleep)
            continue

        if os_path.isfile(pdf_path):
            os_remove(pdf_path)


def mail_report(smtp_recipient, smtp_recipient_name, pdf_path):
    subject_str = 'Your Flu Vaccination Record Receipt'
    if usage_context and (usage_context.lower() != 'prod'):
        subject_str = (
            f'{usage_context} - '
            f'{subject_str}'
        )

    message = MIMEMultipart()
    message['From'] = smtp_sender
    message['To'] = smtp_recipient
    message['Subject'] = subject_str

    if smtp_recipient_name:
        smtp_recipient_name = f' {smtp_recipient_name}'

    body = (
        f'Hello'
        f'{smtp_recipient_name},\n\n'
        f'A receipt containing the details of the flu vaccination '
        f'you received today is attached, in PDF format, '
        f'for your records.\n\n'
        f'Thank you for your help in keeping the '
        f'Duke Community safe and healthy.\n\n'
        f'This email has been automatically generated.\n'
        f'DO NOT REPLY TO THIS EMAIL.\n'
    )

    if usage_context and (usage_context.lower() != 'prod'):
        body = (
            f'{usage_context}\n\n'
            f'{body}'
        )

    message.attach(MIMEText(body, 'plain'))

    with open(pdf_path, 'rb') as pdf:
        pdf_part = MIMEBase('application', 'pdf')
        pdf_part.add_header('Content-Transfer-Encoding', 'base64')
        encoded_pdf = b64encode(pdf.read())
        pdf_part.set_payload(encoded_pdf, 'ascii')
    pdf_part.add_header('Content-Disposition',
                        'attachment; filename="vaccination_receipt.pdf"')
    message.attach(pdf_part)

    text = message.as_string()

    fail_msg = (
        f'Recipient final report send failed to {smtp_recipient}'
    )

    ssl_context = ssl_create_context()
    smtp_retry_count = smtp_retry_global_count
    server = None
    while smtp_retry_count > 0:
        try:
            server = SMTP(smtp_server, smtp_port)
        except Exception as e:
            smtp_log.warning((f'Exception occurred during SMTP setup: '
                              f'{type(e)}'))
            server = None

        if server:
            try:
                server.starttls(context=ssl_context)
                server.sendmail(smtp_sender, smtp_recipient, text)

                success_msg = (
                    f'Recipient final report sent to '
                    f'{smtp_recipient}'
                )
                smtp_log.info(success_msg)
                smtp_retry_count = 0
            except Exception as e:
                smtp_log.warning(fail_msg)
                smtp_log.warning((f'Exception resulting in failure to send: '
                                  f'{type(e)}'))
                smtp_retry_count -= 1
                smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
                continue
            finally:
                server.quit()
        else:
            smtp_log.warning(fail_msg)
            smtp_retry_count -= 1
            smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
            sleep(smtp_retry_global_sleep)
            continue


def mail_clinic_pdf(site_code, pdf_path):
    message = MIMEMultipart()
    message['From'] = smtp_sender
    message['To'] = clinic_distribution_list
    message['Subject'] = f'Clinic QR code PDF for site {site_code}'

    body = (
        f'Please find attached a PDF containing the QR '
        f'code for site {site_code}.\n\n'
        f'This email has been automatically generated.\n'
        f'DO NOT REPLY TO THIS EMAIL.\n'
    )

    message.attach(MIMEText(body, 'plain'))

    site_pdf_content_header = f'attachment; filename="{site_code}.pdf"'
    with open(pdf_path, 'rb') as pdf:
        pdf_part = MIMEBase('application', 'pdf')
        pdf_part.add_header('Content-Transfer-Encoding', 'base64')
        encoded_pdf = b64encode(pdf.read())
        pdf_part.set_payload(encoded_pdf, 'ascii')
    pdf_part.add_header('Content-Disposition',
                        site_pdf_content_header)
    message.attach(pdf_part)

    text = message.as_string()

    fail_msg = (
                f'{pdf_path} send failed to {clinic_distribution_list}'
            )

    ssl_context = ssl_create_context()
    smtp_retry_count = smtp_retry_global_count
    server = None
    while smtp_retry_count > 0:
        try:
            server = SMTP(smtp_server, smtp_port)
        except Exception as e:
            smtp_log.warning((f'Exception occurred during SMTP setup: '
                              f'{type(e)}'))
            server = None

        if server:
            try:
                server.starttls(context=ssl_context)
                server.sendmail(smtp_sender, clinic_distribution_list, text)

                success_msg = f'{pdf_path} sent to {clinic_distribution_list}'
                smtp_log.info(success_msg)
                smtp_retry_count = 0
            except Exception as e:
                smtp_log.warning(fail_msg)
                smtp_log.warning((f'Exception resulting in failure to send: '
                                  f'{type(e)}'))
                smtp_retry_count -= 1
                smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
                continue
            finally:
                server.quit()
        else:
            smtp_log.warning(fail_msg)
            smtp_retry_count -= 1
            smtp_log.warning(f'Retrying {smtp_retry_count} more times.')
            sleep(smtp_retry_global_sleep)
            continue


# Encryption key setup
key_base_path = '/opt/qr_vax_data/keys/'
vaccinator_public_key_file = f'{key_base_path}vaccinator_public.pem'
vaccinator_private_key_file = f'{key_base_path}vaccinator_private.pem'
vaccinatee_public_key_file = f'{key_base_path}vaccinatee_public.pem'
vaccinatee_private_key_file = f'{key_base_path}vaccinatee_private.pem'

with open(vaccinator_public_key_file) as f:
    vaccinator_public_key_text = f.read()
with open(vaccinator_private_key_file) as f:
    vaccinator_private_key_text = f.read()
with open(vaccinatee_public_key_file) as f:
    vaccinatee_public_key_text = f.read()
with open(vaccinatee_private_key_file) as f:
    vaccinatee_private_key_text = f.read()

vaccinator_public_key = RSA.importKey(vaccinator_public_key_text)
vaccinator_private_key = RSA.importKey(vaccinator_private_key_text)
vaccinatee_public_key = RSA.importKey(vaccinatee_public_key_text)
vaccinatee_private_key = RSA.importKey(vaccinatee_private_key_text)

cipher_vaccinator_public = PKCS1_OAEP.new(vaccinator_public_key)
cipher_vaccinator_private = PKCS1_OAEP.new(vaccinator_private_key)
cipher_vaccinatee_public = PKCS1_OAEP.new(vaccinatee_public_key)
cipher_vaccinatee_private = PKCS1_OAEP.new(vaccinatee_private_key)


# Worker pool for processing image generation
imagePoolSize = 4
imageWorkerPool = ThreadPool(imagePoolSize)


def make_qr_image(qr_payload, qr_basename,
                  qr_top_label, qr_bottom_label):
    qr = QRCode()
    qr.add_data(qr_payload)
    qr.make(fit=True)

    base_qr_img = qr.make_image(fill_color='black', back_color='white')
    base_qr_img = base_qr_img.convert('L')
    caption_height = (base_qr_img.height // 3)

    top_caption = Image.new('L',
                            (base_qr_img.width, caption_height),
                            'black')
    draw = ImageDraw.Draw(top_caption)
    _, _, w, h = draw.textbbox((0, 0), qr_top_label, font=qr_topFont)
    draw.text((((top_caption.width - w) // 2),
               ((top_caption.height - h) // 2)),
              qr_top_label,
              font=qr_topFont,
              align='center',
              fill='white')

    bottom_caption = Image.new('L',
                               (base_qr_img.width, caption_height),
                               'black')
    draw = ImageDraw.Draw(bottom_caption)
    _, _, w, h = draw.textbbox((0, 0), qr_bottom_label, font=qr_bottomFont)
    draw.text((((bottom_caption.width - w) // 2),
               ((bottom_caption.height - h) // 2)),
              qr_bottom_label,
              font=qr_bottomFont,
              align='center',
              fill='white')

    qr_img = Image.new('L',
                       (base_qr_img.width,
                        (base_qr_img.height + (2 * caption_height))),
                       'black')
    qr_img.paste(top_caption, (0, 0))
    qr_img.paste(base_qr_img, (0, top_caption.height))
    qr_img.paste(bottom_caption,
                 (0, (top_caption.height + base_qr_img.height)))

    qr_filename = f'{qr_basename}.png'
    qr_path = f'{qr_base_path}{qr_filename}'
    qr_img.save(qr_path)

    top_caption.close()
    bottom_caption.close()
    base_qr_img.close()
    qr_img.close()

    return (qr_filename, qr_path)


def make_vaccinatee_pdf(full_name, qr_timestamp, qr_path, qr_basename):
    generated_dt = qr_timestamp.replace(tzinfo=pytz.utc)
    generated_timestamp = generated_dt.astimezone(local_timezone)
    expired_dt = generated_timestamp + timedelta(hours=survey_valid_hours)
    generated_timestamp_str = generated_timestamp.strftime(time_format)
    expired_timestamp_str = expired_dt.strftime(time_format)

    content = (
        f'<h2>Vaccination QR code for:</h2>'
        f'<section><p><b>{full_name}</b></p></section>'
        f'<h2>QR code generated at:</h2>'
        f'<section><p>{generated_timestamp_str}</p></section>'
        f'<h2>QR code invalid after:</h2>'
        f'<section><p><b>{expired_timestamp_str}</b></p></section>'
        f'<p><img src="file:{qr_path}" '
        f'style="width:8cm;height:auto;"></p>'
    )

    if usage_context and (usage_context.lower() != 'prod'):
        content = (
            f'<h1>{usage_context}<h1>'
            f'{content}'
        )

    pdf_path = f'{pdf_base_path}{qr_basename}.pdf'
    css_str = (
        '@page { '
        'size: Letter; margin: 1.5cm; '
        '@bottom-center { content: url('
        f'"file:{pdf_footer_logo_file}"'
        '); } }'
    )
    html = HTML(string=content)
    css = CSS(string=css_str)
    html.write_pdf(pdf_path, stylesheets=[css])

    return pdf_path


def make_vaccinatee_report(vaccinatee_full_name, vaccinatee_duid,
                           vaccinator_name, vaccinator_duid,
                           clinic_name, clinic_entity,
                           vaccine_brand, vaccine_lot,
                           vaccine_expiration,
                           injection_site,
                           vxn_timestamp):
    generated_dt = vxn_timestamp.replace(tzinfo=pytz.utc)
    generated_timestamp = generated_dt.astimezone(local_timezone)
    generated_timestamp_str = generated_timestamp.strftime(time_format)
    generated_unix_timestamp = generated_dt.timestamp()

    content = (
        f'<h1>Flu Vaccination Record Receipt</h1>'
        f'<section><p>'
        f'<u>Recipient:</u> <b>{vaccinatee_full_name}</b><br />'
        f'<u>Recipient Duke Unique ID:</u> <b>{vaccinatee_duid}</b>'
        f'</section></p>'
        f'<section><p>'
        f'<u>Vaccinator:</u> <b>{vaccinator_name}</b><br />'
        f'<u>Vaccinator Duke Unique ID:</u> <b>{vaccinator_duid}</b>'
        f'</section></p>'
        f'<section><p>'
        f'<u>Clinic:</u> <b>{clinic_name}</b><br />'
        f'<u>Clinic Entity:</u> <b>{clinic_entity}</b>'
        f'</section></p>'
        f'<h2>Vaccine Information</h2>'
        f'<section><p>'
        f'<u>Brand:</u> <b>{vaccine_brand}</b><br />'
        f'<u>Lot #:</u> <b>{vaccine_lot}</b><br />'
        f'<u>Expiration Date:</u> <b>{vaccine_expiration}</b><br />'
        f'<u>Injection Site:</u> <b>{injection_site}</b>'
        f'</section></p>'
        f'<section><p>'
        f'This receipt was generated immediately after successful '
        f'administration of this vaccine, at:<br />'
        f'<b>{generated_timestamp_str}</b><br /><br />'
        f'Thanks so much for helping us <b>Fight the Flu!</b>'
        f'</section></p>'
        f'<p><img src="file:{band_aid_file}" '
        f'style="width:1.5cm;height:auto;"></p>'
    )

    if usage_context and (usage_context.lower() != 'prod'):
        content = (
            f'<h1>{usage_context}<h1>'
            f'{content}'
        )

    pdf_path = (
        f'{pdf_base_path}{vaccinatee_duid}-{generated_unix_timestamp}.pdf'
    )
    css_str = (
        '@page { '
        'size: Letter; margin: 1.5cm; '
        '@bottom-center { content: url('
        f'"file:{pdf_footer_logo_file}"'
        '); } }'
    )
    html = HTML(string=content)
    css = CSS(string=css_str)
    html.write_pdf(pdf_path, stylesheets=[css])

    return pdf_path


def make_vaccinator_pdf(full_name, qr_timestamp, qr_path, qr_basename):
    generated_dt = qr_timestamp.replace(tzinfo=pytz.utc)
    generated_timestamp = generated_dt.astimezone(local_timezone)
    generated_timestamp_str = generated_timestamp.strftime(time_format)

    content = (
        f'<h2>Vaccinator QR code for:</h2>'
        f'<section><p><b>{full_name}</b></p></section>'
        f'<h2>QR code generated at:</h2>'
        f'<section><p>{generated_timestamp_str}</p></section>'
        f'<p><img src="file:{qr_path}" '
        f'style="width:8cm;height:auto;"></p>'
    )

    if usage_context and (usage_context.lower() != 'prod'):
        content = (
            f'<h1>{usage_context}<h1>'
            f'{content}'
        )

    pdf_path = f'{pdf_base_path}{qr_basename}.pdf'
    css_str = (
        '@page { '
        'size: Letter; margin: 1.5cm; '
        '@bottom-center { content: url('
        f'"file:{pdf_footer_logo_file}"'
        '); } }'
    )
    html = HTML(string=content)
    css = CSS(string=css_str)
    html.write_pdf(pdf_path, stylesheets=[css])

    return pdf_path


def make_clinic_pdf(site_code, name, qr_path, qr_basename):
    content = (
        f'<p><img src="file:{qr_path}" '
        f'style="width:10cm;height:auto;"></p>'
        f'<h1>Clinic Name:</h1>'
        f'<strong>{name}</strong>'
    )

    pdf_path = f'{pdf_base_path}{site_code}_{qr_basename}.pdf'
    css_str = '@page { size: Letter; margin: 1.5cm; }'
    html = HTML(string=content)
    css = CSS(string=css_str)
    html.write_pdf(pdf_path, stylesheets=[css])

    return pdf_path


# Worker pool for processing Grouper requests
requestPoolSize = 8
requestWorkerPool = ThreadPool(requestPoolSize)

# Grouper web service credentials and debugging bypass check
grouper_username = environ.get('GROUPER_USERNAME')
grouper_password = environ.get('GROUPER_PASSWORD')
grouper_bypass = environ.get('GROUPER_BYPASS')
grouper_retry_global_count = 5

if not grouper_username:
    base_log.warning('GROUPER_USERNAME is unset!!!')

if not grouper_password:
    base_log.warning('GROUPER_PASSWORD is unset!!!')


def query_grouper(query_url):
    basic = HTTPBasicAuth(grouper_username, grouper_password)

    retval = None
    resp = None
    try:
        resp = request_get(query_url, auth=basic, timeout=4)
    except Exception:
        base_log.warning('Grouper API endpoint not responding.')
        return retval

    status_code = resp.status_code
    headers = resp.headers
    resp.close()

    if status_code == 200:
        result_code = headers.get('X-Grouper-resultCode')
        retval = (result_code == 'IS_MEMBER')
    else:
        base_log.warning('Grouper API endpoint returned bad HTTP code.')

    return retval


def perform_vaccinator_query(duid):
    vaccinator_group = 'eohw-vaccinators'
    student_vaccinator_group = 'student-nurse-vaccinators'
    is_vaccinator_member = None
    is_student_member = None

    # Vaccinator query
    grouper_base_url = (
        f'https://groups.oit.duke.edu'
        f'/grouper-ws/servicesRest/json/v2_5_000/groups/'
        f'duke:policies:eohw-vaccine-administration:'
        f'{vaccinator_group}'
        f'/members/'
    )
    query_url = f'{grouper_base_url}{duid}'
    grouper_retry_count = grouper_retry_global_count
    while (grouper_retry_count > 0) and (is_vaccinator_member is None):
        grouper_retval = query_grouper(query_url)
        if grouper_retval is not None:
            is_vaccinator_member = grouper_retval
        else:
            base_log.warning('Grouper API query for vaccinator failed!')
            grouper_retry_count -= 1
            base_log.warning(f'Retrying {grouper_retry_count} more times.')
            continue
    if is_vaccinator_member is None:
        base_log.warning('Grouper API query failed multiple times!')
        base_log.warning('Please check with IdMS.')
        base_log.warning('Returning fail-safe values.')
        return (False, False)

    # Student vaccinator query
    grouper_base_url = (
        f'https://groups.oit.duke.edu'
        f'/grouper-ws/servicesRest/json/v2_5_000/groups/'
        f'duke:policies:eohw-vaccine-administration:'
        f'{student_vaccinator_group}'
        f'/members/'
    )
    query_url = f'{grouper_base_url}{duid}'
    grouper_retry_count = grouper_retry_global_count
    while (grouper_retry_count > 0) and (is_student_member is None):
        grouper_retval = query_grouper(query_url)
        if grouper_retval is not None:
            is_student_member = grouper_retval
        else:
            base_log.warning('Grouper API query for student failed!')
            grouper_retry_count -= 1
            base_log.warning(f'Retrying {grouper_retry_count} more times.')
            continue
    if is_student_member is None:
        base_log.warning('Grouper API query failed multiple times!')
        base_log.warning('Please check with IdMS.')
        base_log.warning('Returning fail-safe values.')
        return (False, False)

    return (is_vaccinator_member, is_student_member)


def perform_clinic_query(duid):
    clinic_group = 'duke:group-manager:roles:eohw-workflow-support'
    is_member = None

    grouper_base_url = (
        f'https://groups.oit.duke.edu'
        f'/grouper-ws/servicesRest/json/v2_5_000/groups/'
        f'{clinic_group}'
        f'/members/'
    )
    query_url = f'{grouper_base_url}{duid}'
    grouper_retry_count = grouper_retry_global_count
    while (grouper_retry_count > 0) and (is_member is None):
        grouper_retval = query_grouper(query_url)
        if grouper_retval is not None:
            is_member = grouper_retval
        else:
            base_log.warning('Grouper API query for site generation failed!')
            grouper_retry_count -= 1
            base_log.warning(f'Retrying {grouper_retry_count} more times.')
            continue
    if is_member is None:
        base_log.warning('Grouper API query failed multiple times!')
        base_log.warning('Please check with IdMS.')
        base_log.warning('Returning fail-safe value.')
        return False

    return is_member


# SQLAlchemy boilerplate
Base = declarative_base()

# Database access parameters
pg_username = environ.get('PG_USERNAME')
pg_password = environ.get('PG_PASSWORD')
pg_hostname = environ.get('PG_HOSTNAME')
pg_port = environ.get('PG_PORT')
pg_db = environ.get('PG_DATABASE')

if not pg_username:
    base_log.warning('PG_USERNAME is unset!!!')

if not pg_password:
    base_log.warning('PG_USERNAME is unset!!!')

if not pg_hostname:
    base_log.warning('PG_HOSTNAME is unset!!!')

if not pg_port:
    base_log.warning('PG_PORT is unset!!!')

if not pg_db:
    base_log.warning('PG_DB is unset!!!')

pg_connection_string_fmt = (
    'postgresql+pg8000://{username}:{password}@' +
    '{hostname}:{port}/{database}'
)
pg_url = pg_connection_string_fmt.format(
    username=pg_username, password=pg_password,
    hostname=pg_hostname, port=pg_port,
    database=pg_db
)

# Create SqlAlchemy engine for Postgres, with SSL,
# and a sessionmaker
pg_ssl_context = ssl_create_context()
pg_engine = sa_create_engine(pg_url,
                             client_encoding='utf8',
                             connect_args={'ssl_context': pg_ssl_context},
                             pool_size=10,
                             max_overflow=0,
                             pool_recycle=600,
                             pool_pre_ping=True,
                             pool_use_lifo=True,
                             echo=False)
pg_Session = sessionmaker(bind=pg_engine)


# Define the tables we'll be using in Postgres to record what
# comes from Qualtrics.
class VaccinateeQRDataJSON(Base):
    __tablename__ = 'vaccinatee_qr_data_json'
    duid = Column(String(15), index=True)
    qr_id = Column(UUID(as_uuid=True), index=True, primary_key=True)
    source_json = Column(JSONB)

    def __repr__(self):
        return (f'<VaccinateeQRDataJSON(duid={self.duid}, '
                f'qr_id={self.qr_id})>')


class VaccinateeQRData(Base):
    __tablename__ = 'vaccinatee_qr_data'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    duid = Column(String(15), index=True)
    firstName = Column(String(50))
    lastName = Column(String(65))
    email = Column(String(150))
    EggRxn = Column(String(4))
    VaxRxn = Column(String(4))
    GBS = Column(String(4))
    Sick = Column(String(4))
    Consent = Column(String(12))
    qr_id = Column(UUID(as_uuid=True),
                   ForeignKey('vaccinatee_qr_data_json.qr_id'),
                   index=True)
    qr_timestamp = Column(DateTime(timezone=True))

    def __repr__(self):
        return (f'<VaccinateeQRData(duid={self.duid}, qr_id={self.qr_id})>')


class VaccinatorQRDataJSON(Base):
    __tablename__ = 'vaccinator_qr_data_json'
    duid = Column(String(15), index=True)
    qr_id = Column(UUID(as_uuid=True), index=True, primary_key=True)
    source_json = Column(JSONB)

    def __repr__(self):
        return (f'<VaccinatorQRDataJSON(duid={self.duid}, '
                f'qr_id={self.qr_id})>')


class VaccinatorQRData(Base):
    __tablename__ = 'vaccinator_qr_data'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    duid = Column(String(15), index=True)
    firstName = Column(String(50))
    lastName = Column(String(65))
    email = Column(String(150))
    qr_id = Column(UUID(as_uuid=True),
                   ForeignKey('vaccinator_qr_data_json.qr_id'),
                   index=True)
    qr_timestamp = Column(DateTime(timezone=True))

    def __repr__(self):
        return (f'<VaccinatorQRData(duid={self.duid}, qr_id={self.qr_id})>')


class FluVaccinationQRDataJSON(Base):
    __tablename__ = 'flu_vaccination_qr_data_json'
    vaccinatee_duid = Column(String(15), index=True)
    vaccinator_duid = Column(String(15), index=True)
    vxn_id = Column(UUID(as_uuid=True), index=True, primary_key=True)
    source_json = Column(JSONB)

    def __repr__(self):
        return (f'<FluVaccinationQRDataJSON('
                f'vaccinatee_duid={self.vaccinatee_duid}, '
                f'vaccinator_duid={self.vaccinator_duid}, '
                f'vxn_id={self.vxn_id})>')


class FluVaccinationQRData(Base):
    __tablename__ = 'flu_vaccination_qr_data'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)

    vaccinatee_duid = Column(String(15), index=True)
    vaccinatee_firstName = Column(String(50))
    vaccinatee_lastName = Column(String(65))
    vaccinatee_email = Column(String(150))

    vaccinator_duid = Column(String(15), index=True)
    vaccinator_firstName = Column(String(50))
    vaccinator_lastName = Column(String(65))

    clinic_name = Column(String(150), index=True)
    clinic_entity = Column(String(150), index=True)
    clinic_site_code = Column(String(7), index=True)

    vaccine_brand = Column(String(100))
    vaccine_lot = Column(String(100))
    vaccine_expiration = Column(String(20))

    injection_site = Column(String(30))

    vxn_id = Column(UUID(as_uuid=True),
                    ForeignKey('flu_vaccination_qr_data_json.vxn_id'),
                    index=True)
    vxn_timestamp = Column(DateTime(timezone=True))

    def __repr__(self):
        return (f'<FluVaccinationQRData('
                f'vaccinatee_duid={self.vaccinatee_duid}, '
                f'vaccinator_duid={self.vaccinator_duid}, '
                f'vxn_id={self.vxn_id})>')


class SiteData(Base):
    __tablename__ = 'site_data'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    code = Column(String(4), index=True)
    name = Column(String(150))
    entity = Column(String(150))
    qr_basename = Column(UUID(as_uuid=True))
    creation_timestamp = Column(DateTime(timezone=True))

    def __repr__(self):
        return (f'<SiteData(code={self.code}, name=\"{self.name}\")>')


# Set up tables, if not already present.
while True:
    try:
        Base.metadata.create_all(pg_engine)
    except IntegrityError:
        continue
    except SystemExit:
        base_log.info('System exit forced while in table setup loop! '
                      'Investigate database connectivity.')
        break
    except Exception:
        base_log.warning('An unexpected error occurred '
                         'while trying to set up tables!')
        raise
    else:
        break


def put_vaccinatee_db(duid, qr_id, qr_timestamp, qualtrics_data):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            json_record = VaccinateeQRDataJSON(duid=duid,
                                               qr_id=qr_id,
                                               source_json=qualtrics_data)
            session.add(json_record)
            session.commit()

            firstName = qualtrics_data.get('First Name')
            lastName = qualtrics_data.get('Last Name')
            email = qualtrics_data.get('Email')
            egg_rxn = qualtrics_data.get('Egg Rxn')
            vax_rxn = qualtrics_data.get('Vax Rxn')
            GBS = qualtrics_data.get('GBS')
            Sick = qualtrics_data.get('Sick')
            Consent = qualtrics_data.get('Consent')
            qr_record = VaccinateeQRData(duid=duid,
                                         qr_id=qr_id,
                                         qr_timestamp=qr_timestamp,
                                         firstName=firstName,
                                         lastName=lastName,
                                         email=email,
                                         EggRxn=egg_rxn,
                                         VaxRxn=vax_rxn,
                                         GBS=GBS,
                                         Sick=Sick,
                                         Consent=Consent)
            session.add(qr_record)
            session.commit()
            retval = True
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            base_log.warning(f'Encountered a problem performing rollback '
                             f'on backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


def get_vaccinatee_db(duid, qr_id):
    retval = None

    session = None
    try:
        session = pg_Session()
        if session:
            fetch_stmt = select(VaccinateeQRData)
            fetch_stmt = fetch_stmt.where(VaccinateeQRData.duid == duid,
                                          VaccinateeQRData.qr_id == qr_id)
            # For a given duid, qr_id pair, there should only be one result
            retval = session.scalars(fetch_stmt).first()
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


def put_vaccinator_db(duid, qr_id, qr_timestamp, qualtrics_data):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            json_record = VaccinatorQRDataJSON(duid=duid,
                                               qr_id=qr_id,
                                               source_json=qualtrics_data)
            session.add(json_record)
            session.commit()

            firstName = qualtrics_data.get('First Name')
            lastName = qualtrics_data.get('Last Name')
            email = qualtrics_data.get('Email')
            qr_record = VaccinatorQRData(duid=duid,
                                         qr_id=qr_id,
                                         qr_timestamp=qr_timestamp,
                                         firstName=firstName,
                                         lastName=lastName,
                                         email=email)
            session.add(qr_record)
            session.commit()
            retval = True
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            base_log.warning(f'Encountered a problem performing rollback '
                             f'on backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


def get_vaccinator_db(duid, qr_id):
    retval = None

    session = None
    try:
        session = pg_Session()
        if session:
            fetch_stmt = select(VaccinatorQRData)
            fetch_stmt = fetch_stmt.where(VaccinatorQRData.duid == duid,
                                          VaccinatorQRData.qr_id == qr_id)
            # For a given duid, qr_id pair, there should only be one result
            retval = session.scalars(fetch_stmt).first()
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


def put_site_code_db(name, entity, site_code, qr_basename, qr_timestamp):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            site_record = SiteData(code=site_code,
                                   name=name,
                                   entity=entity,
                                   qr_basename=qr_basename,
                                   creation_timestamp=qr_timestamp)
            session.add(site_record)
            session.commit()
            retval = True
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            base_log.warning(f'Encountered a problem performing rollback '
                             f'on backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


def get_site_code_db(site_code):
    retval = None

    session = None
    try:
        session = pg_Session()
        if session:
            fetch_stmt = select(SiteData)
            fetch_stmt = fetch_stmt.where(SiteData.code == site_code)
            # For a given site code, we should only fetch the last one.
            fetch_stmt = fetch_stmt.order_by(desc(SiteData.tbl_id))
            retval = session.scalars(fetch_stmt).first()
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


def put_flu_vaccination_db(vaccinatee_duid, vaccinator_duid,
                           vxn_id, vxn_timestamp, vxn_completed,
                           qualtrics_data):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            json_record = (
                FluVaccinationQRDataJSON(vaccinatee_duid=vaccinatee_duid,
                                         vaccinator_duid=vaccinator_duid,
                                         vxn_id=vxn_id,
                                         source_json=qualtrics_data)
            )
            session.add(json_record)
            session.commit()

            if vxn_completed:
                vaccinatee_FName = (
                    qualtrics_data.get('Vaccinatee First Name')
                )
                vaccinatee_LName = (
                    qualtrics_data.get('Vaccinatee Last Name')
                )
                vaccinatee_email = (
                    qualtrics_data.get('Vaccinatee Email')
                )

                vaccinator_FName = (
                    qualtrics_data.get('Vaccinator First Name')
                )
                vaccinator_LName = (
                    qualtrics_data.get('Vaccinator Last Name')
                )

                clinic_name = qualtrics_data.get('Clinic Name')
                clinic_entity = qualtrics_data.get('Clinic Entity')
                clinic_site_code = qualtrics_data.get('Clinic Site Code')

                vaccine_brand = qualtrics_data.get('Vaccine Brand')
                vaccine_lot = qualtrics_data.get('Vaccine Lot')
                vaccine_expiration = qualtrics_data.get('Vaccine Expiration')

                injection_site = qualtrics_data.get('Injection Site')

                vxn_record = (
                    FluVaccinationQRData(vaccinatee_duid=vaccinatee_duid,
                                         vaccinatee_firstName=vaccinatee_FName,
                                         vaccinatee_lastName=vaccinatee_LName,
                                         vaccinatee_email=vaccinatee_email,
                                         vaccinator_duid=vaccinator_duid,
                                         vaccinator_firstName=vaccinator_FName,
                                         vaccinator_lastName=vaccinator_LName,
                                         clinic_name=clinic_name,
                                         clinic_entity=clinic_entity,
                                         clinic_site_code=clinic_site_code,
                                         vaccine_brand=vaccine_brand,
                                         vaccine_lot=vaccine_lot,
                                         vaccine_expiration=vaccine_expiration,
                                         injection_site=injection_site,
                                         vxn_id=vxn_id,
                                         vxn_timestamp=vxn_timestamp)
                )
                session.add(vxn_record)
                session.commit()
            retval = True
        else:
            base_log.warning('Unable to get connection to '
                             'backend Postgres DB.')
    except Exception as e:
        base_log.warning(f'Encountered a problem communicating '
                         f'with backend Postgres DB: '
                         f'{str(e)}')
        base_log.warning('Exception backtrace follows:')
        base_log.warning(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            base_log.warning(f'Encountered a problem performing rollback '
                             f'on backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            base_log.warning(f'Encountered a problem closing connection '
                             f'with backend Postgres DB: '
                             f'{str(e)}')
            base_log.warning('Exception backtrace follows:')
            base_log.warning(format_exc())

    return retval


# The body of the QR vaccination web app
app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = '/opt/qr_vax_data/credentials/htpasswd'
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

htpasswd = HtPasswdAuth(app)


def get_qr_code(qualtrics_data, qr_type):
    duid = qualtrics_data.get('duid')
    qr_id = uuid4()
    qr_timestamp = datetime.utcnow()

    db_insert = False
    if duid:
        # Duplicating code here, in order to get good debugging information.
        if qr_type == QRCodeType.Vaccinatee:
            db_insert = put_vaccinatee_db(duid, qr_id,
                                          qr_timestamp, qualtrics_data)
            if not db_insert:
                base_log.warning(f'Database insertion failure for: '
                                 f'{duid}:{qr_id}')
                return (None, 'Database Insertion Failure')
        elif qr_type == QRCodeType.Vaccinator:
            # First, check with Grouper if we're a vaccinator,
            # but only if we're not bypassing checks for debugging.
            vaccinator_member, student_member = requestWorkerPool.spawn(
                perform_vaccinator_query,
                duid
            ).get()
            base_log.info((f'Vaccinator membership for {duid} '
                           f'is: {vaccinator_member}'))
            base_log.info((f'Student vaccinator membership for {duid} '
                           f'is: {student_member}'))

            if (
                    (not vaccinator_member) and
                    (not student_member) and
                    (not grouper_bypass)
            ):
                return (None, 'Unauthorized')

            # Next, attempt to insert into database.
            db_insert = put_vaccinator_db(duid, qr_id,
                                          qr_timestamp, qualtrics_data)
            if not db_insert:
                base_log.warning(f'Database insertion failure for: '
                                 f'{duid}:{qr_id}')
                return (None, 'Database Insertion Failure')

    first_name = qualtrics_data.get('First Name')
    last_name = qualtrics_data.get('Last Name')
    email = qualtrics_data.get('Email')

    full_name = ''
    if first_name and last_name:
        full_name = f'{first_name} {last_name}'
    elif first_name:
        full_name = first_name
    elif last_name:
        full_name = last_name
    else:
        full_name = f'Duke Unique ID: {duid}'

    qr_basename = f'{duid}:{qr_id}'
    qr_data = bytes(qr_basename, 'utf-8')

    # RSA encrypt QR data
    enc_qr_data = None
    if qr_type == QRCodeType.Vaccinatee:
        enc_qr_data = cipher_vaccinatee_public.encrypt(qr_data)
    elif qr_type == QRCodeType.Vaccinator:
        enc_qr_data = cipher_vaccinator_public.encrypt(qr_data)
    b64_enc_qr_data = urlsafe_b64encode(enc_qr_data).decode('utf-8')

    # Construct QR payload
    qr_payload = (
        f'https://{qr_hostname}/qr/v3/{usage_context.lower()}/'
        f'{qr_type}/{b64_enc_qr_data}'
    )

    qr_num_str = '3'
    qr_label = 'VACCINATEE'
    qr_pdf_func = make_vaccinatee_pdf
    if qr_type == QRCodeType.Vaccinator:
        qr_num_str = '2'
        qr_label = 'VACCINATOR'
        qr_pdf_func = make_vaccinator_pdf
    if usage_context and (usage_context.lower() != 'prod'):
        qr_label = f'{qr_label}\n({usage_context})'

    qr_filename, qr_path = imageWorkerPool.spawn(
        make_qr_image,
        qr_payload,
        qr_basename,
        qr_num_str,
        qr_label
    ).get()

    pdf_path = imageWorkerPool.spawn(
        qr_pdf_func,
        full_name,
        qr_timestamp,
        qr_path,
        qr_basename
    ).get()

    # Queue sending a copy to the email on record,
    # if available.
    if email:
        qr_mail_queue.put((email, full_name, qr_timestamp,
                           qr_type,
                           qr_path, pdf_path))

    return (qr_filename, None)


def get_qr_payload(qr_data, qr_type):
    qr_payload = qr_data.get('qr_payload')
    if not qr_payload:
        base_log.warning('QR payload was empty!')
        base_log.warning('Data seen was:')
        base_log.warning(qr_data)
        return (None, 'Missing Payload')

    path_components = PurePosixPath(
        unquote(
            urlparse(
                qr_payload
            ).path
        )
    )

    qr_environment = None
    try:
        qr_environment = path_components.parts[3]
    except IndexError:
        base_log.warning('QR payload wrongly formed!')
        base_log.warning('Data seen was:')
        base_log.warning(qr_data)
        return (None, 'Missing Environment')

    if (usage_context.lower() != qr_environment):
        return (None, 'Wrong Environment')

    b64_enc_qr_payload = None
    try:
        b64_enc_qr_payload = path_components.parts[5]
    except IndexError:
        base_log.warning('QR payload wrongly formed!')
        base_log.warning('Data seen was:')
        base_log.warning(qr_data)
        return (None, 'Missing Data')

    try:
        enc_qr_payload = urlsafe_b64decode(b64_enc_qr_payload)
    except Exception:
        base_log.warning('QR payload could not be decoded!')
        base_log.warning('Data seen was:')
        base_log.warning(qr_data)
        return (None, 'Could Not Decode Data')

    # RSA decrypt QR payload
    qr_payload_bytes = None
    try:
        if qr_type == QRCodeType.Vaccinatee:
            qr_payload_bytes = (
                cipher_vaccinatee_private.decrypt(enc_qr_payload)
            )
        elif qr_type == QRCodeType.Vaccinator:
            qr_payload_bytes = (
                cipher_vaccinator_private.decrypt(enc_qr_payload)
            )
    except ValueError:
        base_log.warning('QR payload decryption failure!')
        base_log.warning('Data seen was:')
        base_log.warning(qr_data)
        return (None, 'Could Not Decrypt Data')

    duid = None
    qr_id = None
    qr_payload = None
    if qr_payload_bytes:
        qr_payload = qr_payload_bytes.decode('utf-8').strip()
    try:
        duid, qr_id = qr_payload.split(':')
    except ValueError:
        base_log.warning('QR payload could not be split!')
        base_log.warning('Payload seen was:')
        base_log.warning(qr_payload)
        return (None, 'Decrypted Data Wrongly Formatted')

    # Duplicating code here, in order to get good debugging information.
    db_record = None
    db_reason = None
    if qr_type == QRCodeType.Vaccinatee:
        db_record = get_vaccinatee_db(duid, qr_id)
        if not db_record:
            base_log.warning(f'Database query failed or '
                             f'did not find record for '
                             f'vaccinatee: {duid}:{qr_id}')
            db_reason = 'No Record'
    elif qr_type == QRCodeType.Vaccinator:
        db_record = get_vaccinator_db(duid, qr_id)
        if not db_record:
            base_log.warning(f'Database query failed or '
                             f'did not find record for '
                             f'vaccinator: {duid}:{qr_id}')
            db_reason = 'No Record'

    return (db_record, db_reason)


@app.route('/api/v1/vaccinatee_qr_encode', methods=['POST'])
@htpasswd.required
def get_vaccinatee_qr_code(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    headers = {'Content-Type': 'application/json'}
    response_data = {}
    response_data['succeeded'] = False

    qualtrics_data = request.json
    if qualtrics_data:
        qr_filename, _ = get_qr_code(qualtrics_data,
                                     QRCodeType.Vaccinatee)

        if qr_filename:
            response_data['qr_code'] = qr_filename
            response_data['succeeded'] = True

    return make_response(json_dump(response_data),
                         200, headers)


@app.route('/api/v1/vaccinatee_qr_decode', methods=['POST'])
@htpasswd.required
def get_vaccinatee_qr_payload(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
    }
    payload_json = {}
    payload_json['succeeded'] = False

    qr_data = request.json
    if qr_data:
        vaccinatee_db_record, status = (
            get_qr_payload(qr_data, QRCodeType.Vaccinatee)
        )
        if vaccinatee_db_record:
            qr_timestamp = vaccinatee_db_record.qr_timestamp
            generated_dt = qr_timestamp.replace(tzinfo=pytz.utc)
            generated_timestamp = generated_dt.astimezone(local_timezone)
            generated_timestamp_str = generated_timestamp.strftime(time_format)
            expired_dt = (
                generated_timestamp + timedelta(hours=survey_valid_hours)
            )
            expired_timestamp_str = expired_dt.strftime(time_format)

            payload_json['First Name'] = vaccinatee_db_record.firstName
            payload_json['Last Name'] = vaccinatee_db_record.lastName
            payload_json['duid'] = vaccinatee_db_record.duid
            payload_json['Email'] = vaccinatee_db_record.email
            payload_json['Code Expiration Time'] = expired_timestamp_str
            payload_json['Code Generation Time'] = generated_timestamp_str
            payload_json['Sick'] = vaccinatee_db_record.Sick
            payload_json['Egg Rxn'] = vaccinatee_db_record.EggRxn
            payload_json['Vax Rxn'] = vaccinatee_db_record.VaxRxn
            payload_json['GBS'] = vaccinatee_db_record.GBS
            payload_json['Consent'] = vaccinatee_db_record.Consent
            payload_json['succeeded'] = True
        else:
            payload_json['failure_reason'] = status

    return make_response(json_dump(payload_json),
                         200, headers)


@app.route('/api/v1/vaccinatee_mail_final_report', methods=['POST'])
@htpasswd.required
def send_vaccinatee_final_report_pdf(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    qualtrics_data = request.json
    if qualtrics_data:
        vxn_id = uuid4()
        vxn_timestamp = datetime.utcnow()

        vaccinatee_first_name = qualtrics_data.get('Vaccinatee First Name')
        vaccinatee_last_name = qualtrics_data.get('Vaccinatee Last Name')
        vaccinatee_duid = qualtrics_data.get('Vaccinatee DUID')
        vaccinatee_email = qualtrics_data.get('Vaccinatee Email')

        vaccinatee_full_name = ''
        if vaccinatee_first_name and vaccinatee_last_name:
            vaccinatee_full_name = (
                f'{vaccinatee_first_name} '
                f'{vaccinatee_last_name}'
            )
        elif vaccinatee_first_name:
            vaccinatee_full_name = vaccinatee_first_name
        elif vaccinatee_last_name:
            vaccinatee_full_name = vaccinatee_last_name
        else:
            vaccinatee_full_name = (
                f'Duke Unique ID: '
                f'{vaccinatee_duid}'
            )

        vaccinator_first_name = qualtrics_data.get('Vaccinator First Name')
        vaccinator_last_name = qualtrics_data.get('Vaccinator Last Name')
        vaccinator_first_initial = (
            f'{vaccinator_first_name[0]}. ' if vaccinator_first_name else ''
        )
        vaccinator_name = (
            f'{vaccinator_first_initial}'
            f'{vaccinator_last_name}'
        )
        vaccinator_duid = qualtrics_data.get('Vaccinator DUID')

        clinic_name = qualtrics_data.get('Clinic Name')
        clinic_entity = qualtrics_data.get('Clinic Entity')

        vaccine_brand = qualtrics_data.get('Vaccine Brand')
        vaccine_lot = qualtrics_data.get('Vaccine Lot')
        vaccine_expiration = qualtrics_data.get('Vaccine Expiration')

        injection_site = qualtrics_data.get('Injection Site')

        vxn_completed = False
        vxn_completed_str = qualtrics_data.get('Vaccination Completed')
        if vxn_completed_str.lower() == 'true':
            vxn_completed = True

        db_insert = False
        if vaccinatee_duid and vaccinator_duid:
            db_insert = put_flu_vaccination_db(vaccinatee_duid,
                                               vaccinator_duid,
                                               vxn_id,
                                               vxn_timestamp,
                                               vxn_completed,
                                               qualtrics_data)
        if not db_insert:
            no_insert_msg = (
                f'Database records for recipient {vaccinatee_duid} '
                f'and vaccinator {vaccinator_duid} not inserted!\n'
                f'Check Qualtrics for data, and investigate cause!\n'
            )
            base_log.warning(no_insert_msg)

        if not vxn_completed:
            incomplete_vxn_msg = (
                f'Vaccinator {vaccinator_duid} cancelled vaccination '
                f'for vaccinatee {vaccinatee_duid}; recorded in database.'
            )
            base_log.info(incomplete_vxn_msg)
            return make_response('Cancelled vaccination recorded.\n', 200)

        pdf_path = imageWorkerPool.spawn(
            make_vaccinatee_report,
            vaccinatee_full_name,
            vaccinatee_duid,
            vaccinator_name,
            vaccinator_duid,
            clinic_name, clinic_entity,
            vaccine_brand, vaccine_lot,
            vaccine_expiration,
            injection_site,
            vxn_timestamp
        ).get()

        if vaccinatee_email:
            # Queue sending the report, if an email is available.
            report_mail_queue.put((vaccinatee_email,
                                   vaccinatee_full_name,
                                   pdf_path))
        else:
            # Log that the report was not sent.
            no_email_msg = (
                f'Recipient {vaccinatee_duid} has no email on record; '
                f'final report not sent.'
            )
            smtp_log.warning(no_email_msg)

        return make_response('Final report sent.\n', 200)
    else:
        return make_response('Required data not provided.\n', 400)


@app.route('/api/v1/vaccinator_qr_encode', methods=['POST'])
@htpasswd.required
def get_vaccinator_qr_code(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    headers = {'Content-Type': 'application/json'}
    response_data = {}
    response_data['succeeded'] = False

    qualtrics_data = request.json
    if qualtrics_data:
        qr_filename, failure_status = get_qr_code(qualtrics_data,
                                                  QRCodeType.Vaccinator)

        if qr_filename:
            response_data['qr_code'] = qr_filename
            response_data['succeeded'] = True

        if failure_status:
            response_data['failure_reason'] = failure_status

    return make_response(json_dump(response_data),
                         200, headers)


@app.route('/api/v1/vaccinator_qr_decode', methods=['POST'])
@htpasswd.required
def get_vaccinator_qr_payload(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
    }
    payload_json = {}
    payload_json['succeeded'] = False

    qr_data = request.json
    if qr_data:
        vaccinator_db_record, status = (
            get_qr_payload(qr_data, QRCodeType.Vaccinator)
        )
        if vaccinator_db_record:
            # Check if we're still in the appropriate groups,
            # and report status to the front end.
            # We can still bypass checks for debugging.
            duid = vaccinator_db_record.duid
            vaccinator_member, student_member = requestWorkerPool.spawn(
                perform_vaccinator_query,
                duid
            ).get()
            base_log.info((f'Vaccinator membership for {duid} '
                           f'is: {vaccinator_member}'))
            base_log.info((f'Student vaccinator membership for {duid} '
                           f'is: {student_member}'))

            if (vaccinator_member or student_member) or grouper_bypass:
                payload_json['First Name'] = vaccinator_db_record.firstName
                payload_json['Last Name'] = vaccinator_db_record.lastName
                payload_json['duid'] = duid
                payload_json['Email'] = vaccinator_db_record.email
                payload_json['succeeded'] = True

                if student_member:
                    payload_json['student'] = True
                else:
                    payload_json['student'] = False
            else:
                payload_json['failure_reason'] = 'Unauthorized'
        else:
            payload_json['failure_reason'] = status

    return make_response(json_dump(payload_json),
                         200, headers)


@app.route('/api/v1/check_clinic_creation_permissions', methods=['POST'])
@htpasswd.required
def get_clinic_perms(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
    }
    response_data = {}
    response_data['authorized'] = False
    response_data['succeeded'] = False

    qualtrics_data = request.json
    if qualtrics_data:
        duid = qualtrics_data.get('duid')
        if duid:
            # Check if we're still in the appropriate groups,
            # and report status to the front end.
            # We can still bypass checks for debugging.
            is_member = requestWorkerPool.spawn(
                perform_clinic_query,
                duid
            ).get()
            base_log.info((f'Clinic generation membership for {duid} '
                           f'is: {is_member}'))

            if is_member or grouper_bypass:
                response_data['authorized'] = True

            response_data['succeeded'] = True

    return make_response(json_dump(response_data),
                         200, headers)


@app.route('/api/v1/generate_clinic_qr_code', methods=['POST'])
@htpasswd.required
def generate_clinic_qr_code(user):
    if request.method != 'POST':
        base_log.warning('A non-POST request was attempted!')
        return make_response('Malformed request.\n', 400)

    qualtrics_data = request.json
    if qualtrics_data:
        qr_basename = uuid4()
        qr_timestamp = datetime.utcnow()

        name = qualtrics_data.get('Clinic Name')
        entity = qualtrics_data.get('Clinic Entity')
        site_code = qualtrics_data.get('Clinic Site Code')

        db_insert = False
        if site_code:
            db_insert = put_site_code_db(name, entity, site_code,
                                         qr_basename, qr_timestamp)
        if not db_insert:
            return make_response('Database insertion failure.\n', 400)

        qr_payload = (
            f'https://{qr_hostname}/qr/v3/site/'
            f'{QRCodeType.Clinic}/{site_code}'
        )

        qr_filename, qr_path = imageWorkerPool.spawn(
            make_qr_image,
            qr_payload,
            qr_basename,
            '4',
            f'SITE CODE\n{site_code}'
        ).get()

        pdf_path = imageWorkerPool.spawn(
            make_clinic_pdf,
            site_code,
            name,
            qr_path,
            qr_basename
        ).get()

        if clinic_distribution_list:
            # Queue sending the clinic PDF
            clinic_mail_queue.put((site_code, pdf_path))
        else:
            # Log that the clinic QR PDF was not sent.
            no_clinic_email_msg = (
                f'Clinic distribution list not set! '
                f'PDF {pdf_path} for site {site_code} not sent.'
            )
            smtp_log.warning(no_clinic_email_msg)

        return make_response('Clinic QR code generated.\n', 200)
    else:
        return make_response('Required data not provided.\n', 400)


@app.route('/api/v1/site_code_lookup/<string:site_code>', methods=['GET'])
def get_clinic_information(site_code):
    if request.method != 'GET':
        base_log.warning('A non-GET request was attempted!')
        return make_response('Malformed request.\n', 400)

    headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Cache-Control': 'no-cache'
    }
    payload_json = {}
    payload_json['succeeded'] = False

    if site_code:
        site_db_record = get_site_code_db(site_code)
        if site_db_record:
            payload_json['site_code'] = site_db_record.code
            payload_json['name'] = site_db_record.name
            payload_json['entity'] = site_db_record.entity
            payload_json['succeeded'] = True

    return make_response(json_dump(payload_json),
                         200, headers)


@app.route('/api/v1/eohw_vax_qr_online', methods=['GET'])
def online():
    if request.method != 'GET':
        return make_response('Malformed request.\n', 400)

    current_dt_str = str(datetime.utcnow().isoformat('#'))
    response_str = f'Online - {current_dt_str}\n'

    return make_response(response_str, 200)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
