#!/usr/bin/env python3

from csv import reader as csvreader
from json import dumps as json_dump

from qrcode import QRCode
from pydf import generate_pdf

qr_directory = '/home/vjo/Downloads/site_codes/qr_codes/'
pdf_directory = '/home/vjo/Downloads/site_codes/site_pdfs/'


with open('clinics2.csv') as csvfile:
    csvrd = csvreader(csvfile, delimiter=';')
    for row in csvrd:
        site_code = int(row[0])
        entity = 'Duke Health'
        name = row[1]

        site_dict = {}
        site_dict['site_code'] = site_code
        site_dict['entity'] = entity
        site_dict['name'] = name
        site_dict['type'] = 2

        qr = QRCode()
        qr_path = f'{qr_directory}{site_code}.png'
        qr.add_data(json_dump(site_dict))
        qr.make(fit=True)
        qr_img = qr.make_image(fill_color='black', back_color='white')
        qr_img.save(qr_path)

        pdf_content = (
            f'<img src="{qr_path}" height=500 width=500><br/>'
            f'<h1>Site Code: {site_code}</h1>'
            f'<h2>Clinic Name:</h2>'
            f'<strong>{name}</strong>'
        )

        pdf_path = f'{pdf_directory}{site_code}.pdf'
        pdf = generate_pdf(pdf_content,
                           grayscale=True,
                           image_quality=100,
                           page_size='Letter')
        with open(pdf_path, 'wb') as f:
            f.write(pdf)
