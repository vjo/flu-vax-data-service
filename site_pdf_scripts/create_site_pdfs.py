#!/usr/bin/env python3

import sys

from os import environ
from os.path import isdir, exists

from dotenv import load_dotenv

from csv import reader as csvreader

from qrcode import QRCode
from uuid import uuid4
from datetime import datetime

from ssl import create_default_context as ssl_create_context

from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy import create_engine as sa_create_engine
from sqlalchemy import Column, Identity, Integer, String
from sqlalchemy import DateTime

from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

from weasyprint import HTML, CSS

from PIL import Image, ImageFont, ImageDraw

from traceback import format_exc

load_dotenv()

# SQLAlchemy boilerplate
Base = declarative_base()

# Database access parameters
pg_username = environ.get('PG_USERNAME')
pg_password = environ.get('PG_PASSWORD')
pg_hostname = environ.get('PG_HOSTNAME')
pg_port = environ.get('PG_PORT')
pg_db = environ.get('PG_DATABASE')

if not pg_username:
    print('PG_USERNAME is unset! Exiting.')
    sys.exit()

if not pg_password:
    print('PG_USERNAME is unset! Exiting.')
    sys.exit()

if not pg_hostname:
    print('PG_HOSTNAME is unset! Exiting.')
    sys.exit()

if not pg_port:
    print('PG_PORT is unset! Exiting.')
    sys.exit()

if not pg_db:
    print('PG_DB is unset! Exiting.')
    sys.exit()

pg_connection_string_fmt = (
    'postgresql+pg8000://{username}:{password}@' +
    '{hostname}:{port}/{database}'
)
pg_url = pg_connection_string_fmt.format(
    username=pg_username, password=pg_password,
    hostname=pg_hostname, port=pg_port,
    database=pg_db
)

# Create SqlAlchemy engine for Postgres, with SSL,
# and a sessionmaker
pg_ssl_context = ssl_create_context()
pg_engine = sa_create_engine(pg_url,
                             client_encoding='utf8',
                             connect_args={'ssl_context': pg_ssl_context},
                             pool_size=5,
                             max_overflow=0,
                             pool_recycle=600,
                             pool_pre_ping=True,
                             pool_use_lifo=True,
                             echo=False)
pg_Session = sessionmaker(bind=pg_engine)


class SiteData(Base):
    __tablename__ = 'site_data'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    code = Column(String(4), index=True)
    name = Column(String(150))
    entity = Column(String(150))
    qr_basename = Column(UUID(as_uuid=True))
    creation_timestamp = Column(DateTime(timezone=True))

    def __repr__(self):
        return (f'<SiteData(code={self.code}, name=\"{self.name}\")>')


def put_site_code_db(name, entity, site_code, qr_basename, qr_timestamp):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            site_record = SiteData(code=site_code,
                                   name=name,
                                   entity=entity,
                                   qr_basename=qr_basename,
                                   creation_timestamp=qr_timestamp)
            session.add(site_record)
            session.commit()
            retval = True
        else:
            print('Unable to get connection to '
                  'backend Postgres DB.')
    except Exception as e:
        print(f'Encountered a problem communicating '
              f'with backend Postgres DB: '
              f'{str(e)}')
        print('Exception backtrace follows:')
        print(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            print(f'Encountered a problem performing rollback '
                  f'on backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            print(f'Encountered a problem closing connection '
                  f'with backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    return retval


pdf_base_path = './site_pdfs/'
qr_base_path = './qr_codes/'
qr_font = '/usr/share/fonts/truetype/noto/NotoSans-Bold.ttf'
qr_topFont = ImageFont.truetype(qr_font, 80)
qr_bottomFont = ImageFont.truetype(qr_font, 40)
clinic_qr_code_type = 2


def make_clinic_qr_image(site_code, qr_payload, qr_basename):
    qr = QRCode()
    qr.add_data(qr_payload)
    qr.make(fit=True)

    top_message = '4'
    bottom_message = f'SITE CODE\n{site_code}'

    base_qr_img = qr.make_image(fill_color='black', back_color='white')
    base_qr_img = base_qr_img.convert('L')
    caption_height = (base_qr_img.height // 3)

    top_caption = Image.new('L',
                            (base_qr_img.width, caption_height),
                            'black')
    draw = ImageDraw.Draw(top_caption)
    _, _, w, h = draw.textbbox((0, 0), top_message, font=qr_topFont)
    draw.text((((top_caption.width - w) // 2),
               ((top_caption.height - h) // 2)),
              top_message,
              font=qr_topFont,
              align='center',
              fill='white')

    bottom_caption = Image.new('L',
                               (base_qr_img.width, caption_height),
                               'black')
    draw = ImageDraw.Draw(bottom_caption)
    _, _, w, h = draw.textbbox((0, 0), bottom_message, font=qr_bottomFont)
    draw.text((((bottom_caption.width - w) // 2),
               ((bottom_caption.height - h) // 2)),
              bottom_message,
              font=qr_bottomFont,
              align='center',
              fill='white')

    qr_img = Image.new('L',
                       (base_qr_img.width,
                        (base_qr_img.height + (2 * caption_height))),
                       'black')
    qr_img.paste(top_caption, (0, 0))
    qr_img.paste(base_qr_img, (0, top_caption.height))
    qr_img.paste(bottom_caption,
                 (0, (top_caption.height + base_qr_img.height)))

    qr_filename = f'{qr_basename}.png'
    qr_path = f'{qr_base_path}{qr_filename}'
    qr_img.save(qr_path)

    return qr_path


def make_clinic_pdf(site_code, name, qr_path, qr_basename):
    content = (
        f'<p><img src="file:{qr_path}" '
        f'style="width:10cm;height:auto;"></p>'
        f'<h1>Clinic Name:</h1>'
        f'<strong>{name}</strong>'
    )

    pdf_path = f'{pdf_base_path}{site_code}_{qr_basename}.pdf'
    css_str = '@page { size: Letter; margin: 1.5cm; }'
    html = HTML(string=content)
    css = CSS(string=css_str)
    html.write_pdf(pdf_path, stylesheets=[css])

    return pdf_path


# What hostname should we provide in the QR codes?
qr_hostname = environ.get('QR_HOSTNAME')
if not qr_hostname:
    print('QR_HOSTNAME is unset! Exiting.')
    sys.exit()

if not isdir(qr_base_path):
    print(f'{qr_base_path} does not exist! Exiting.')
    sys.exit()

if not isdir(pdf_base_path):
    print(f'{pdf_base_path} does not exist! Exiting.')
    sys.exit()

if not exists(qr_font):
    print(f'Could not find {qr_font}! Install and try again.')
    sys.exit()

skip_files = environ.get('SKIP_FILES')
if skip_files:
    print('SKIP_FILES was set')
    print('Will insert into database, but not create files.')

with open('clinics.csv') as csvfile:
    csvrd = csvreader(csvfile, delimiter='|')
    for row in csvrd:
        site_code = int(row[0])
        entity = row[1]
        name = row[2]

        qr_payload = (
            f'https://{qr_hostname}/qr/v3/site/'
            f'{clinic_qr_code_type}/{site_code}'
        )
        qr_basename = uuid4()
        qr_timestamp = datetime.utcnow()

        db_insert = put_site_code_db(name, entity, site_code,
                                     qr_basename, qr_timestamp)
        if not db_insert:
            print((f'Unable to insert database record for site code: '
                   f'{site_code}'))
        else:
            print(f'Inserted database record for site code: {site_code}')
            if not skip_files:
                qr_path = make_clinic_qr_image(site_code,
                                               qr_payload,
                                               qr_basename)
                pdf_path = make_clinic_pdf(site_code,
                                           name,
                                           qr_path,
                                           qr_basename)
                print(f'Clinic PDF created for {site_code} in: {pdf_path}')
